package com.dong.netty;

import com.dong.handler.MessageDecoder;
import com.dong.handler.MessageEncoder;
import com.dong.handler.PacketHandler;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * create by s.he
 * on 2017/12/1 0001
 */
@Component
@ConfigurationProperties(prefix = "netty")
public class NioService extends ChannelInitializer<SocketChannel> {
    private boolean traceFrame = false;

    private int readTimeout = 60;

    private boolean enableKeepClientAlive = false;

    @Autowired
    private PacketHandler packetHandler;


    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("ReadTimeoutDetector", new ReadTimeoutHandler(readTimeout));

        if (enableKeepClientAlive) {
            pipeline.addLast("KeepAliver", new IdleStateHandler(readTimeout / 3, readTimeout / 3, readTimeout / 3));
        }

        String delimiter = "$$$";
        pipeline.addLast(new DelimiterBasedFrameDecoder(1000, Unpooled.copiedBuffer(delimiter.getBytes())));
        pipeline.addLast(new MessageDecoder());
        pipeline.addLast(packetHandler);
        pipeline.addLast(new MessageEncoder());
    }
}
