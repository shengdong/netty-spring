package com.dong.netty;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import java.util.ArrayList;
import java.util.List;

/**
 * create by s.he
 * on 2017/12/1 0001
 */
public final class NioEventFactory {
    private static List<EventLoopGroup> lists = new ArrayList<>(2);

    static  {
        new Thread(() -> shutDown()).start();
    }

    public static EventLoopGroup createEventLoopGroup() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        lists.add(eventLoopGroup);
        return eventLoopGroup;
    }

    public static  void shutDown() {
        for(EventLoopGroup eventExecutor : lists) {
            eventExecutor.shutdownGracefully();
        }
    }
}
