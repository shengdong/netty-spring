package com.dong.netty;

/**
 * create by s.he
 * on 2017/12/1 0001
 */
public interface NettyStarter {

    void run();
}
