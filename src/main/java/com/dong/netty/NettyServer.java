package com.dong.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * create by s.he
 * on 2017/12/1 0001
 */
@Component
@ConfigurationProperties(prefix = "netty")
public class NettyServer implements NettyStarter{

    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    private Integer port = 8000;

    private String address = "127.0.0.1";

    private int connectTimeout = 300000;

    private int socketBacklog = 128;

    private boolean noDelay = true;

    private boolean socketKeepAlive = true;

    private boolean enablePoolBuffer = true;

    private int sendBufferSize = 32768;

    private int receiveBufferSize = 32768;


    private ServerBootstrap serverBootstrap;

    @Autowired
    private NioService nioService;



    @Override
    public void run() {
        EventLoopGroup bossGroup = NioEventFactory.createEventLoopGroup();
        EventLoopGroup workGroup = NioEventFactory.createEventLoopGroup();
        serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workGroup);
        serverBootstrap.channel(NioServerSocketChannel.class);
        serverBootstrap.childHandler(nioService);

        serverBootstrap.option(ChannelOption.SO_BACKLOG, getSocketBacklog());
        serverBootstrap.option(ChannelOption.TCP_NODELAY, isNoDelay());
        serverBootstrap.option(ChannelOption.SO_KEEPALIVE, isSocketKeepAlive());
        serverBootstrap.option(ChannelOption.SO_SNDBUF, getSendBufferSize());
        serverBootstrap.option(ChannelOption.SO_RCVBUF, getReceiveBufferSize());
        if (isEnablePoolBuffer()) {
            serverBootstrap.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            serverBootstrap.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        }

        serverBootstrap.childOption(ChannelOption.CONNECT_TIMEOUT_MILLIS, getConnectTimeout());

        try {
            ChannelFuture f = serverBootstrap.bind(getPort()).sync();
            f.channel().closeFuture().sync();
        } catch (Exception e) {
            logger.error("Unexpected error is occurred while running lock platform.", e);
        } finally {
            NioEventFactory.shutDown();
        }
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getSocketBacklog() {
        return socketBacklog;
    }

    public void setSocketBacklog(int socketBacklog) {
        this.socketBacklog = socketBacklog;
    }

    public boolean isNoDelay() {
        return noDelay;
    }

    public void setNoDelay(boolean noDelay) {
        this.noDelay = noDelay;
    }

    public boolean isSocketKeepAlive() {
        return socketKeepAlive;
    }

    public void setSocketKeepAlive(boolean socketKeepAlive) {
        this.socketKeepAlive = socketKeepAlive;
    }

    public boolean isEnablePoolBuffer() {
        return enablePoolBuffer;
    }

    public void setEnablePoolBuffer(boolean enablePoolBuffer) {
        this.enablePoolBuffer = enablePoolBuffer;
    }

    public int getSendBufferSize() {
        return sendBufferSize;
    }

    public void setSendBufferSize(int sendBufferSize) {
        this.sendBufferSize = sendBufferSize;
    }

    public int getReceiveBufferSize() {
        return receiveBufferSize;
    }

    public void setReceiveBufferSize(int receiveBufferSize) {
        this.receiveBufferSize = receiveBufferSize;
    }
}
